const txtFirstName= document.querySelector('#txt-first-name');
const txtLastName= document.querySelector('#txt-last-name')
const spanFullName= document.querySelector('#span-full-name')
// Alternatives for document.querySelector()
/*
	document.getElementById('txt-firstName');
	document.getElementByClassName('txt-inputs')
	document.getElementByTagName('input');
*/

// Event listener listens to a specific event 
// 'keyup' event indicates which is key is pressed
// (event) parameter is an event handler property

const getFullName = () => {
	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;
	
	spanFullName.innerHTML = firstName + ' ' + lastName;
}
txtLastName.addEventListener('keyup', getFullName);
txtFirstName.addEventListener('keyup', getFullName);

